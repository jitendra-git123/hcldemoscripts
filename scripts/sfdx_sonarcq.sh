#!/bin/bash
##This is the script to send sonar success/failed notifications to individual developers
##And also it Kill the CI build based on sonar analaysis Failed
chmod +x ./${DEVOPS_SCM_FOLDER}/scripts/sonar-scanner.sh
mkdir -p ${CI_PROJECT_DIR}/target/.sonar
./${DEVOPS_SCM_FOLDER}/scripts/sonar-scanner.sh
/tmp/sonar-scanner-4.6.0.2311-linux/bin/sonar-scanner \
      -Dsonar.host.url=$SONAR_URL \
      -Dsonar.projectKey=hclaccdemo \
      -Dsonar.projectName=hclaccdemo \
      -Dsonar.projectVersion=$CI_PIPELINE_ID \
      -Dsonar.projectBaseDir=${CI_PROJECT_DIR}/${SFDC_SCM_FOLDER} \
      -Dsonar.sources=./force-app \
      -Dsonar.login=059ed195b9f98b2f1501ac4c4ed8a1c0105b4931 \
      -Dsonar.skipPackageDesign=true \
      -Dsonar.ce.javaOpts=-Xmx2048m \
      -Dsonar.working.directory=${CI_PROJECT_DIR}/target/.sonar \
      -Dsonar.web.javaOpts=-Xmx2048m
ls -lrt
pwd
cd ${CI_PROJECT_DIR}/target/.sonar && ls -lrt
cat report-task.txt
########New code lines
export url=$(cat ${CI_PROJECT_DIR}/target/.sonar/report-task.txt | grep ceTaskUrl | cut -c11- )
echo $url
sleep 10
curl -k -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $url -o ${CI_PROJECT_DIR}/target/.sonar/analysis.txt
export status=$(cat ${CI_PROJECT_DIR}/target/.sonar/analysis.txt | jq -r '.task.status')
analysisId=$(curl -XGET -s -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $url | jq -r '.task.analysisId')
I=1
TIMEOUT=0
while [ "$analysisId" == "null" ]
do
  if [ "$TIMEOUT" -gt 300 ]
  then
    echo "Timeout of " + $TIMEOUT + " seconds exceeded for getting analysisId"
    exit 1
  fi
  sleep $I
  TIMEOUT=$((TIMEOUT+I))
  I=$((I+1))
  analysisId=$(curl -XGET -s -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $url | jq -r '.task.analysisId')
done
STATUS=$(curl -XGET -s -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $url | jq -r '.task.status')
if [ "$STATUS" == "SUCCESS" ];then
   echo -e "SONAR ANALYSIS SUCCESSFUL…ANALYSING RESULTS"
   curl -k -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $SONAR_URL/api/qualitygates/project_status?analysisId=$analysisId -o ${CI_PROJECT_DIR}/target/.sonar/result.txt
   export result=$(cat ${CI_PROJECT_DIR}/target/.sonar/result.txt | jq -r '.projectStatus.status')
   if [ "$result" == "ERROR" ];then
      echo -e "SONAR RESULTS FAILED"
      #prints the critical, major and minor violation
      curl -k -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $SONAR_URL/api/qualitygates/project_status?analysisId=$analysisId | jq -r '.projectStatus.conditions' > ${CI_PROJECT_DIR}/target/.sonar/sqgresult.json
      cat ${CI_PROJECT_DIR}/target/.sonar/sqgresult.json
      export Output1=$(awk -v RS= '{$1=$1}1' ${CI_PROJECT_DIR}/target/.sonar/sqgresult.json)
      exit 1 #breaks the build for violations
   else
      echo -e "SONAR RESULTS SUCCESSFUL"
      #echo "$(cat ${CI_PROJECT_DIR}/target/.sonar/result.txt | jq -r '.projectStatus.conditions')"
      cat ${CI_PROJECT_DIR}/target/.sonar/result.txt | jq -r '.projectStatus.conditions'
      curl -k -u "059ed195b9f98b2f1501ac4c4ed8a1c0105b4931:" $url | jq -r '.task' > ${CI_PROJECT_DIR}/target/.sonar/output.json
      cat ${CI_PROJECT_DIR}/target/.sonar/output.json
      export Output2=$(awk -v RS= '{$1=$1}1' ${CI_PROJECT_DIR}/target/.sonar/output.json)
      exit 0
   fi
else
   echo -e "SONAR ANALYSIS FAILED"
   exit 1
fi

