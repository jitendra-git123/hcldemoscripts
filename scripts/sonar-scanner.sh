#!/bin/bash
mkdir /tmp -p
cd /tmp
chmod 755 /tmp
wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.0.2311-linux.zip
unzip sonar-scanner-cli-4.6.0.2311-linux.zip
mkdir /tmp/sonar-scanner/ -p
echo "sonar.host.url=http://20.186.14.126:9000 \n sonar.sourceEncoding=UTF-8" > /tmp/sonar-scanner-4.6.0.2311-linux/conf/sonar-scanner.properties
export PATH="$PATH:/tmp/sonar-scanner-4.6.0.2311-linux/bin"
sonar-scanner -v
