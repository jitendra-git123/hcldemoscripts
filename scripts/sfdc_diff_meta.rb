#
#!/usr/bin/ruby

##===== INCLUDING THE RUBYGEMS, RUBY LIBS, DEPENDENCY RUBY SCRIPTS =====##

require 'rubygems'
require 'json'
require 'fileutils'
require 'yaml'
# Ruby Script Name: Comparison of git tag and updated the same
baselineTAG, gitlabTestBranch = "",""
ARGV.each do |arg|
            (baselineTAG = arg.split("=")[1]) if arg.split("=")[0] =~ /baselineTAG/i
            (gitlabTestBranch = arg.split("=")[1]) if arg.split("=")[0] =~ /gitlabTestBranch/i
            (CI_CDPATH = arg.split("=")[1]) if arg.split("=")[0] =~ /CI_CDPATH/i
end
`pwd`
`ls -lrt`

_sourcepath = "#{CI_CDPATH}"
_destinationpath = "#{CI_CDPATH}".gsub(/sfdx_da_metada/, '')
puts "#{_destinationpath}"
###Git difference tag and HEAD
`echo "--- package differences ---"`
FileUtils.cd("#{_sourcepath}", :verbose => true)
#`git fetch https://${DEVOPS_REPO_USERNAME1}:${DEVOPS_REPO_TOKEN1}@${SFDC_SCM_REPO}`
puts "list tag"
`git fetch https://${DEVOPS_REPO_USERNAME1}:${DEVOPS_REPO_TOKEN1}@${SFDC_SCM_REPO} --tags`
`git tag| grep R17.0`
puts "========"
`ls -lrt`

puts "git diff-tree -r #{baselineTAG}..HEAD --no-commit-id --name-only | xargs -I'{}' tar -rvf ../files.tar '{}'"
`git diff-tree -r #{baselineTAG}..HEAD --no-commit-id --name-only | xargs -I'{}' tar -rvf ../files.tar '{}'`
`ls -lrt`
`git diff-tree -r #{baselineTAG}..HEAD --no-commit-id --name-only > ../gitdiff.txt`

FileUtils.cp_r "../files.tar", "#{CI_CDPATH}"
`tar -xvf ../files.tar -C ../`
`cat ../gitdiff.txt`
f = open("../gitdiff.txt")
    contents_array = []
    filearray = []
    listoffiles = []
    actualvalue = []
    aurallwc = []
    f.each_line { |line| contents_array << line  unless line =~ /.gitlab-ci.yml/ }
f.close
contents_array.each do |line|
    filearray <<  line.split("/")
end
filearray.each do |file|
   puts "#{file}"
   firstdir = file[0]
   puts file[0]
   seconddir = file[1]
   puts file[0]
   listoffiles << file[-1]
   objtrsn = file[-2]
   objtrsnfile = file[3]
   if objtrsnfile == "objectTranslations"
      objtrans = objtrsn + ".objectTranslation-meta.xml"
      find = `find #{_sourcepath} -type f -name '#{objtrans}' ! -name '.gitlab-ci.yml'`
      actualvalue << find.split("\n")
   elsif (objtrsnfile == "aura" || objtrsnfile == "lwc")
      puts "#{file[-2]}"  
	  puts "#{_sourcepath}/#{file[0]}/#{file[1]}/#{file[2]}/#{file[3]}/#{file[-2]}"
      findvalue = `find #{_sourcepath}/#{file[0]}/#{file[1]}/#{file[2]}/#{file[3]}/#{file[-2]} -type f -name '#{file[-2]}*' ! -name '.gitlab-ci.yml'`	 
      actualvalue << findvalue.split("\n")
   else
      puts "No value"
   end
end

listoffiles.each do |compare|
  comp = "#{compare}".chomp
  append = comp + "-meta.xml"
  puts "finding a file under #{_sourcepath}"
  find = `find #{_sourcepath} -type f -name '#{append}' ! -name '.gitlab-ci.yml'`
  actualvalue <<  find.split("\n")
end
actualvalue.each do |actual|
   actual.each do |actual1|
     filearray.each do |file|
        firstdir = file[0]
        seconddir = file[1]
        replace ="#{actual1}".gsub(/sfdx_da_metada/, '')
        FileUtils.cp_r "#{actual1}", "#{replace}"
     end
   end
end

FileUtils.cd("#{_destinationpath}", :verbose => true)
`ls -lrt ../`
`tar -cvf #{_destinationpath}/changesets.tar force-app/`
